-- MariaDB dump 10.17  Distrib 10.4.8-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: newjavon
-- ------------------------------------------------------
-- Server version	10.4.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actress`
--

DROP TABLE IF EXISTS `actress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `name_kanji` varchar(50) NOT NULL,
  `name_hiragana` varchar(50) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actress`
--

LOCK TABLES `actress` WRITE;
/*!40000 ALTER TABLE `actress` DISABLE KEYS */;
INSERT INTO `actress` VALUES (3,'-ajSr65c.jpg','Actress 1','Actress 1','','2020-07-29 18:02:59','2020-07-29 18:02:59'),(4,'-fUljDLX.jpg','Actress 2','Actress 2','','2020-07-29 18:03:13','2020-07-29 18:03:13');
/*!40000 ALTER TABLE `actress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advertisement`
--

DROP TABLE IF EXISTS `advertisement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertisement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `code` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertisement`
--

LOCK TABLES `advertisement` WRITE;
/*!40000 ALTER TABLE `advertisement` DISABLE KEYS */;
/*!40000 ALTER TABLE `advertisement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth`
--

DROP TABLE IF EXISTS `auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `source` varchar(255) NOT NULL,
  `source_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-auth-user_id` (`user_id`),
  CONSTRAINT `fk-auth-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth`
--

LOCK TABLES `auth` WRITE;
/*!40000 ALTER TABLE `auth` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('Super Admin','1',1595495519);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('/*',2,NULL,NULL,NULL,1595495605,1595495605),('/actress/*',2,NULL,NULL,NULL,1595564594,1595564594),('/actress/create',2,NULL,NULL,NULL,1595570561,1595570561),('/actress/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/actress/index',2,NULL,NULL,NULL,1595570561,1595570561),('/actress/update',2,NULL,NULL,NULL,1595570561,1595570561),('/actress/view',2,NULL,NULL,NULL,1595570561,1595570561),('/advertisement/*',2,NULL,NULL,NULL,1595570561,1595570561),('/advertisement/create',2,NULL,NULL,NULL,1595570561,1595570561),('/advertisement/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/advertisement/index',2,NULL,NULL,NULL,1595570561,1595570561),('/advertisement/update',2,NULL,NULL,NULL,1595570561,1595570561),('/advertisement/view',2,NULL,NULL,NULL,1595570561,1595570561),('/contact/*',2,NULL,NULL,NULL,1595570561,1595570561),('/contact/create',2,NULL,NULL,NULL,1595570561,1595570561),('/contact/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/contact/index',2,NULL,NULL,NULL,1595570561,1595570561),('/contact/update',2,NULL,NULL,NULL,1595570561,1595570561),('/contact/view',2,NULL,NULL,NULL,1595570561,1595570561),('/dashboard/index',2,NULL,NULL,NULL,1595584151,1595584151),('/director/*',2,NULL,NULL,NULL,1595570561,1595570561),('/director/create',2,NULL,NULL,NULL,1595570561,1595570561),('/director/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/director/index',2,NULL,NULL,NULL,1595570561,1595570561),('/director/update',2,NULL,NULL,NULL,1595570561,1595570561),('/director/view',2,NULL,NULL,NULL,1595570561,1595570561),('/genre/*',2,NULL,NULL,NULL,1595570561,1595570561),('/genre/create',2,NULL,NULL,NULL,1595570561,1595570561),('/genre/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/genre/index',2,NULL,NULL,NULL,1595570561,1595570561),('/genre/update',2,NULL,NULL,NULL,1595570561,1595570561),('/genre/view',2,NULL,NULL,NULL,1595570561,1595570561),('/label/*',2,NULL,NULL,NULL,1595570561,1595570561),('/label/create',2,NULL,NULL,NULL,1595570561,1595570561),('/label/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/label/index',2,NULL,NULL,NULL,1595570561,1595570561),('/label/update',2,NULL,NULL,NULL,1595570561,1595570561),('/label/view',2,NULL,NULL,NULL,1595570561,1595570561),('/manufacturer/*',2,NULL,NULL,NULL,1595570561,1595570561),('/manufacturer/create',2,NULL,NULL,NULL,1595570561,1595570561),('/manufacturer/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/manufacturer/index',2,NULL,NULL,NULL,1595570561,1595570561),('/manufacturer/update',2,NULL,NULL,NULL,1595570561,1595570561),('/manufacturer/view',2,NULL,NULL,NULL,1595570561,1595570561),('/series/*',2,NULL,NULL,NULL,1595570561,1595570561),('/series/create',2,NULL,NULL,NULL,1595570561,1595570561),('/series/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/series/index',2,NULL,NULL,NULL,1595570561,1595570561),('/series/update',2,NULL,NULL,NULL,1595570561,1595570561),('/series/view',2,NULL,NULL,NULL,1595570561,1595570561),('/setting/*',2,NULL,NULL,NULL,1595931125,1595931125),('/setting/director/create',2,NULL,NULL,NULL,1595931568,1595931568),('/setting/director/delete',2,NULL,NULL,NULL,1595931591,1595931591),('/setting/director/index',2,NULL,NULL,NULL,1595931600,1595931600),('/setting/director/update',2,NULL,NULL,NULL,1595931606,1595931606),('/setting/director/view',2,NULL,NULL,NULL,1595931610,1595931610),('/setting/genre/*',2,NULL,NULL,NULL,1595931674,1595931674),('/setting/genre/create',2,NULL,NULL,NULL,1595931718,1595931718),('/setting/genre/delete',2,NULL,NULL,NULL,1595931724,1595931724),('/setting/genre/index',2,NULL,NULL,NULL,1595931731,1595931731),('/setting/genre/update',2,NULL,NULL,NULL,1595931734,1595931734),('/setting/genre/view',2,NULL,NULL,NULL,1595931743,1595931743),('/setting/label/*',2,NULL,NULL,NULL,1595931769,1595931769),('/setting/label/create',2,NULL,NULL,NULL,1595931775,1595931775),('/setting/label/delete',2,NULL,NULL,NULL,1595931780,1595931780),('/setting/label/index',2,NULL,NULL,NULL,1595931786,1595931786),('/setting/label/update',2,NULL,NULL,NULL,1595931791,1595931791),('/setting/label/view',2,NULL,NULL,NULL,1595931798,1595931798),('/setting/manufacturer/*',2,NULL,NULL,NULL,1595931819,1595931819),('/setting/manufacturer/create',2,NULL,NULL,NULL,1595931851,1595931851),('/setting/manufacturer/delete',2,NULL,NULL,NULL,1595931857,1595931857),('/setting/manufacturer/index',2,NULL,NULL,NULL,1595931861,1595931861),('/setting/manufacturer/update',2,NULL,NULL,NULL,1595931866,1595931866),('/setting/manufacturer/view',2,NULL,NULL,NULL,1595931870,1595931870),('/setting/series/*',2,NULL,NULL,NULL,1595931902,1595931902),('/setting/series/create',2,NULL,NULL,NULL,1595931909,1595931909),('/setting/series/delete',2,NULL,NULL,NULL,1595931913,1595931913),('/setting/series/index',2,NULL,NULL,NULL,1595931916,1595931916),('/setting/series/update',2,NULL,NULL,NULL,1595931922,1595931922),('/setting/series/view',2,NULL,NULL,NULL,1595931925,1595931925),('/setting/studio/*',2,NULL,NULL,NULL,1595931942,1595931942),('/setting/studio/create',2,NULL,NULL,NULL,1595931947,1595931947),('/setting/studio/delete',2,NULL,NULL,NULL,1595931950,1595931950),('/setting/studio/index',2,NULL,NULL,NULL,1595931954,1595931954),('/setting/studio/update',2,NULL,NULL,NULL,1595931959,1595931959),('/setting/studio/view',2,NULL,NULL,NULL,1595931962,1595931962),('/setting/tag/*',2,NULL,NULL,NULL,1595932018,1595932018),('/setting/tag/create',2,NULL,NULL,NULL,1595932022,1595932022),('/setting/tag/delete',2,NULL,NULL,NULL,1595932029,1595932029),('/setting/tag/index',2,NULL,NULL,NULL,1595932034,1595932034),('/setting/tag/update',2,NULL,NULL,NULL,1595932042,1595932042),('/setting/tag/view',2,NULL,NULL,NULL,1595932049,1595932049),('/setting/type/*',2,NULL,NULL,NULL,1595932062,1595932062),('/setting/type/create',2,NULL,NULL,NULL,1595932070,1595932070),('/setting/type/delete',2,NULL,NULL,NULL,1595932073,1595932073),('/setting/type/index',2,NULL,NULL,NULL,1595932076,1595932076),('/setting/type/update',2,NULL,NULL,NULL,1595932081,1595932081),('/setting/type/view',2,NULL,NULL,NULL,1595932085,1595932085),('/studio/*',2,NULL,NULL,NULL,1595570561,1595570561),('/studio/create',2,NULL,NULL,NULL,1595570561,1595570561),('/studio/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/studio/index',2,NULL,NULL,NULL,1595570561,1595570561),('/studio/update',2,NULL,NULL,NULL,1595570561,1595570561),('/studio/view',2,NULL,NULL,NULL,1595570561,1595570561),('/subscriber/*',2,NULL,NULL,NULL,1595570561,1595570561),('/subscriber/create',2,NULL,NULL,NULL,1595570561,1595570561),('/subscriber/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/subscriber/index',2,NULL,NULL,NULL,1595570561,1595570561),('/subscriber/subscriber/*',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscriber/create',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscriber/delete',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscriber/index',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscriber/update',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscriber/view',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscription/*',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscription/create',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscription/delete',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscription/index',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscription/update',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/subscription/view',2,NULL,NULL,NULL,1596016871,1596016871),('/subscriber/update',2,NULL,NULL,NULL,1595570561,1595570561),('/subscriber/view',2,NULL,NULL,NULL,1595570561,1595570561),('/subscription/*',2,NULL,NULL,NULL,1595570561,1595570561),('/subscription/create',2,NULL,NULL,NULL,1595570561,1595570561),('/subscription/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/subscription/index',2,NULL,NULL,NULL,1595570561,1595570561),('/subscription/update',2,NULL,NULL,NULL,1595570561,1595570561),('/subscription/view',2,NULL,NULL,NULL,1595570561,1595570561),('/tag/*',2,NULL,NULL,NULL,1595570561,1595570561),('/tag/create',2,NULL,NULL,NULL,1595570561,1595570561),('/tag/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/tag/index',2,NULL,NULL,NULL,1595570561,1595570561),('/tag/update',2,NULL,NULL,NULL,1595570561,1595570561),('/tag/view',2,NULL,NULL,NULL,1595570561,1595570561),('/type/*',2,NULL,NULL,NULL,1595570561,1595570561),('/type/create',2,NULL,NULL,NULL,1595570561,1595570561),('/type/delete',2,NULL,NULL,NULL,1595570561,1595570561),('/type/index',2,NULL,NULL,NULL,1595570561,1595570561),('/type/update',2,NULL,NULL,NULL,1595570561,1595570561),('/type/view',2,NULL,NULL,NULL,1595570561,1595570561),('/video/*',2,NULL,NULL,NULL,1595570421,1595570421),('/video/create',2,NULL,NULL,NULL,1595570460,1595570460),('/video/delete',2,NULL,NULL,NULL,1595570460,1595570460),('/video/index',2,NULL,NULL,NULL,1595570460,1595570460),('/video/update',2,NULL,NULL,NULL,1595570460,1595570460),('/video/upload',2,NULL,NULL,NULL,1595573532,1595573532),('/video/video/create',2,NULL,NULL,NULL,1595930243,1595930243),('/video/video/delete',2,NULL,NULL,NULL,1595930243,1595930243),('/video/video/index',2,NULL,NULL,NULL,1595930243,1595930243),('/video/video/update',2,NULL,NULL,NULL,1595930243,1595930243),('/video/video/upload',2,NULL,NULL,NULL,1595930243,1595930243),('/video/video/view',2,NULL,NULL,NULL,1595930243,1595930243),('/video/view',2,NULL,NULL,NULL,1595570460,1595570460),('Super Admin',1,NULL,NULL,NULL,1595495490,1595495649);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('Super Admin','/*');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `director`
--

DROP TABLE IF EXISTS `director`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `director` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `director`
--

LOCK TABLES `director` WRITE;
/*!40000 ALTER TABLE `director` DISABLE KEYS */;
/*!40000 ALTER TABLE `director` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (12,'DS3nKHUM.png','Shinra','2020-07-30 07:04:13','2020-07-30 07:04:13'),(13,'nFKEPVY8.jpg','Hinata','2020-07-30 07:04:28','2020-07-30 07:04:28');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `label`
--

DROP TABLE IF EXISTS `label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `label` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `label`
--

LOCK TABLES `label` WRITE;
/*!40000 ALTER TABLE `label` DISABLE KEYS */;
/*!40000 ALTER TABLE `label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturer`
--

LOCK TABLES `manufacturer` WRITE;
/*!40000 ALTER TABLE `manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Actresses',NULL,'/actress/index',4,'account'),(3,'Advertisements',NULL,'/advertisement/index',12,'picture-in-picture'),(4,'Contacts',NULL,'/contact/index',13,'info'),(5,'Directors',NULL,'/setting/director/index',5,'accounts-alt'),(6,'Genres',NULL,'/setting/genre/index',2,'folder-star'),(7,'Labels',NULL,'/setting/label/index',8,'label'),(8,'Manufacturers',NULL,'/setting/manufacturer/index',9,'group'),(9,'Series',NULL,'/setting/series/index',6,'collection-case-play'),(10,'Studios',NULL,'/setting/studio/index',7,'city'),(11,'Subscribers',NULL,'/subscriber/subscriber/index',10,'account-box'),(12,'Subscriptions',NULL,'/subscriber/subscription/index',11,'folder-person'),(13,'Tags',NULL,'/setting/tag/index',3,'tag'),(14,'Types',NULL,'/setting/type/index',3,'group-work'),(15,'Videos',NULL,'/video/video/index',1,'videocam'),(16,'Video Import',NULL,'/video/upload',14,'upload'),(17,'Dashboard',NULL,'/dashboard/index',NULL,'view-dashboard');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1595485896),('m130524_201442_init',1595485898),('m180821_065113_create_auth_table',1595485898),('m181127_093611_alter_user_table',1595485898),('m200722_025415_create_tag_table',1595923306),('m200722_025620_create_actress_table',1595923306),('m200722_030646_create_genre_table',1595923306),('m200722_031150_create_director_table',1595923306),('m200722_031242_create_manufacturer_table',1595923306),('m200722_031310_create_label_table',1595923306),('m200722_031312_create_type_table',1595923306),('m200722_031405_create_series_table',1595923306),('m200722_032118_create_studio_table',1595923306),('m200722_032901_create_video_table',1595923307),('m200722_032903_create_video_genre_table',1595923307),('m200722_032904_create_video_tag_table',1595923307),('m200722_032905_create_video_actress_table',1595923308),('m200722_035135_create_subscriber_table',1595923308),('m200722_035136_create_subscriber_history_table',1595923308),('m200722_040312_create_subscription_table',1595923308),('m200722_041715_create_subscription_payment_table',1595923308),('m200722_042717_create_video_rating_table',1595923308),('m200722_042740_create_video_subscriber_favorite_table',1595923309),('m200722_042802_create_video_subscriber_like_table',1595923309),('m200722_042816_create_video_subscriber_unlike_table',1595923309),('m200722_042931_create_video_comment_table',1595923309),('m200722_043005_create_video_view_table',1595923310),('m200722_043454_create_contact_table',1595923310),('m200722_043638_create_advertisement_table',1595923310);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `series`
--

DROP TABLE IF EXISTS `series`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `series` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `series`
--

LOCK TABLES `series` WRITE;
/*!40000 ALTER TABLE `series` DISABLE KEYS */;
/*!40000 ALTER TABLE `series` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studio`
--

DROP TABLE IF EXISTS `studio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studio`
--

LOCK TABLES `studio` WRITE;
/*!40000 ALTER TABLE `studio` DISABLE KEYS */;
/*!40000 ALTER TABLE `studio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriber`
--

DROP TABLE IF EXISTS `subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `avatar_url` varchar(255) DEFAULT NULL,
  `end_of_subscription` varchar(255) DEFAULT NULL,
  `cc_type` varchar(255) DEFAULT NULL,
  `last_four_cc_number` varchar(4) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `auth_key` varchar(50) DEFAULT NULL,
  `access_token` varchar(50) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriber`
--

LOCK TABLES `subscriber` WRITE;
/*!40000 ALTER TABLE `subscriber` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscriber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriber_history`
--

DROP TABLE IF EXISTS `subscriber_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_id` int(11) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-subscriber_history-subscriber_id` (`subscriber_id`),
  CONSTRAINT `fk-subscriber_history-subscriber_id` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriber_history`
--

LOCK TABLES `subscriber_history` WRITE;
/*!40000 ALTER TABLE `subscriber_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscriber_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `no_of_months` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` double NOT NULL,
  `is_approved` tinyint(1) DEFAULT NULL,
  `is_popular` tinyint(1) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `expiration_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription_payment`
--

DROP TABLE IF EXISTS `subscription_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_no` varchar(100) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-subscription_payment-subscription_id` (`subscription_id`),
  KEY `idx-subscription_payment-subscriber_id` (`subscriber_id`),
  CONSTRAINT `fk-subscription_payment-subscriber_id` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-subscription_payment-subscription_id` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription_payment`
--

LOCK TABLES `subscription_payment` WRITE;
/*!40000 ALTER TABLE `subscription_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dp_url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin',NULL,NULL,'$2y$13$Hyc5grEYG1HF7bn562kKG.dqviD/kmXkyIrzNEHaI1PuGnba6/1TO',NULL,'neovash23@gmail.com',10,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `duration` time NOT NULL,
  `director_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `label_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `series_id` int(11) DEFAULT NULL,
  `studio_id` int(11) DEFAULT NULL,
  `product_code` varchar(30) NOT NULL,
  `release_date` date DEFAULT NULL,
  `part_number` varchar(30) NOT NULL,
  `product_description` text NOT NULL,
  `video_url` varchar(255) NOT NULL,
  `preview_url` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `small_cover_image_url` varchar(255) DEFAULT NULL,
  `large_cover_image_url` varchar(255) DEFAULT NULL,
  `small_thumbnail_url` varchar(255) DEFAULT NULL,
  `large_thumbnail_url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-video-director_id` (`director_id`),
  KEY `idx-video-manufacturer_id` (`manufacturer_id`),
  KEY `idx-video-label_id` (`label_id`),
  KEY `idx-video-type_id` (`type_id`),
  KEY `idx-video-series_id` (`series_id`),
  KEY `idx-video-studio_id` (`studio_id`),
  CONSTRAINT `fk-video-director_id` FOREIGN KEY (`director_id`) REFERENCES `director` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video-label_id` FOREIGN KEY (`label_id`) REFERENCES `label` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video-manufacturer_id` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturer` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video-series_id` FOREIGN KEY (`series_id`) REFERENCES `series` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video-studio_id` FOREIGN KEY (`studio_id`) REFERENCES `studio` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video-type_id` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
INSERT INTO `video` VALUES (14,'asdasdasdsad','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasd',NULL,'asdasdasdas','asdasdasdsadasd','i2BOaMqX.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 18:13:56','2020-07-29 18:13:56'),(15,'asdasdasdsad','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasd',NULL,'asdasdasdas','asdasdasdsadasd','9R2oToFc.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 18:15:34','2020-07-29 18:15:34'),(16,'asdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasda',NULL,'sdasdasdasdas','asdasdasdasdas','LFx_F2zB.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 18:17:38','2020-07-29 18:17:38'),(17,'asdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasda',NULL,'sdasdasdasdas','asdasdasdasdas','CKrcIHWX.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 18:23:27','2020-07-29 18:23:27'),(18,'asdasdasdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdas','dsadasdasdas','juJYVDYZ.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 18:27:46','2020-07-29 18:27:46'),(19,'asdasdasdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdas','dsadasdasdas','wgzYCDWU.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 18:32:23','2020-07-29 18:32:23'),(20,'asdasdasdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdas','dsadasdasdas','iyHlMNc9.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 18:33:21','2020-07-29 18:33:21'),(21,'asdasdasdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdas','dsadasdasdas','f4LxMDwn.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 18:34:18','2020-07-29 18:34:18'),(22,'asdasdasdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdas','dsadasdasdas','DISqV-5u.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 18:36:19','2020-07-29 18:36:19'),(23,'Test with actress','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdas','dsadasdasdas','p92oA8u4.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 18:36:56','2020-07-29 19:48:42'),(24,'asdasdsadas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdasdas',NULL,'dasdasdasdasdasd','asdasdasdasdasdasd','xZnViTb2.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 19:49:12','2020-07-29 19:49:12'),(25,'asdasdasds','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdasd',NULL,'asdasdas','asdasdasd','0fnACgDb.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 19:51:03','2020-07-29 19:51:03'),(26,'asdasdasdasdasd','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdasd',NULL,'asdasd','asdasdasdasd','MGhWC3Gq.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 19:54:19','2020-07-29 19:54:19'),(27,'asdasdasdasdasd','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdadasdasdasdasd','asdasdasdasdasdasd','6Ko5Vzb2.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 19:56:13','2020-07-29 19:56:13'),(28,'asdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdasd','asdasdasdasdsa','7QCc4BjL.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 19:57:39','2020-07-29 19:57:39'),(29,'asdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdasd','asdasdasdasdsa','EZgoJ5oX.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 19:58:01','2020-07-29 19:58:01'),(30,'asdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdasd','asdasdasdasdsa','NexC-pPI.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 19:58:55','2020-07-29 19:58:55'),(31,'asdasdas','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdasd','asdasdasdasdsa','0yUxnRHM.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 19:59:28','2020-07-29 19:59:28'),(32,'32','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas','2020-07-26','dasdasdasdasdas','asdasdasdasd','ujo8sSTb.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-29 20:00:40','2020-07-29 20:20:42'),(33,'asdasdasdasdasdsa','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdasd','asdasdasdasd','hvi2_PoW.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-30 07:12:56','2020-07-30 07:12:56'),(34,'test 1','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdasdas','asdasdasdasd','LmeAiUpM.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-30 07:14:35','2020-07-30 07:14:35'),(35,'test 1','00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,'asdasdas',NULL,'dasdasdasdas','asdasdasdasd','_88J8RG5.mp4',NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-30 07:15:59','2020-07-30 07:22:39');
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_actress`
--

DROP TABLE IF EXISTS `video_actress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_actress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `actress_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-video_actress-video_id` (`video_id`),
  KEY `idx-video_actress-actress_id` (`actress_id`),
  CONSTRAINT `fk-video_actress-actress_id` FOREIGN KEY (`actress_id`) REFERENCES `actress` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video_actress-video_id` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_actress`
--

LOCK TABLES `video_actress` WRITE;
/*!40000 ALTER TABLE `video_actress` DISABLE KEYS */;
INSERT INTO `video_actress` VALUES (13,31,4,'2020-07-29 19:59:28','2020-07-29 19:59:28'),(28,32,3,'2020-07-29 20:20:42','2020-07-29 20:20:42'),(29,32,4,'2020-07-29 20:20:42','2020-07-29 20:20:42'),(30,33,3,'2020-07-30 07:12:56','2020-07-30 07:12:56'),(31,33,4,'2020-07-30 07:12:56','2020-07-30 07:12:56'),(32,34,3,'2020-07-30 07:14:35','2020-07-30 07:14:35'),(33,34,4,'2020-07-30 07:14:35','2020-07-30 07:14:35'),(38,35,4,'2020-07-30 07:22:39','2020-07-30 07:22:39');
/*!40000 ALTER TABLE `video_actress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_comment`
--

DROP TABLE IF EXISTS `video_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-video_comment-video_id` (`video_id`),
  KEY `idx-video_comment-subscriber_id` (`subscriber_id`),
  CONSTRAINT `fk-video_comment-subscriber_id` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video_comment-video_id` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_comment`
--

LOCK TABLES `video_comment` WRITE;
/*!40000 ALTER TABLE `video_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_genre`
--

DROP TABLE IF EXISTS `video_genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-video_genre-video_id` (`video_id`),
  KEY `idx-video_genre-genre_id` (`genre_id`),
  CONSTRAINT `fk-video_genre-genre_id` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video_genre-video_id` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_genre`
--

LOCK TABLES `video_genre` WRITE;
/*!40000 ALTER TABLE `video_genre` DISABLE KEYS */;
INSERT INTO `video_genre` VALUES (9,35,12,'2020-07-30 07:22:39','2020-07-30 07:22:39'),(10,35,13,'2020-07-30 07:22:39','2020-07-30 07:22:39'),(11,35,12,'2020-07-30 07:22:39','2020-07-30 07:22:39'),(12,35,13,'2020-07-30 07:22:39','2020-07-30 07:22:39'),(13,35,12,'2020-07-30 07:22:39','2020-07-30 07:22:39'),(14,35,13,'2020-07-30 07:22:39','2020-07-30 07:22:39'),(15,35,12,'2020-07-30 07:22:39','2020-07-30 07:22:39'),(16,35,13,'2020-07-30 07:22:39','2020-07-30 07:22:39');
/*!40000 ALTER TABLE `video_genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_rating`
--

DROP TABLE IF EXISTS `video_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `ratings` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-video_rating-video_id` (`video_id`),
  KEY `idx-video_rating-subscriber_id` (`subscriber_id`),
  CONSTRAINT `fk-video_rating-subscriber_id` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video_rating-video_id` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_rating`
--

LOCK TABLES `video_rating` WRITE;
/*!40000 ALTER TABLE `video_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_subscriber_favorite`
--

DROP TABLE IF EXISTS `video_subscriber_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_subscriber_favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-video_subscriber_favorite-video_id` (`video_id`),
  KEY `idx-video_subscriber_favorite-subscriber_id` (`subscriber_id`),
  CONSTRAINT `fk-video_subscriber_favorite-subscriber_id` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video_subscriber_favorite-video_id` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_subscriber_favorite`
--

LOCK TABLES `video_subscriber_favorite` WRITE;
/*!40000 ALTER TABLE `video_subscriber_favorite` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_subscriber_favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_subscriber_like`
--

DROP TABLE IF EXISTS `video_subscriber_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_subscriber_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-video_subscriber_like-video_id` (`video_id`),
  KEY `idx-video_subscriber_like-subscriber_id` (`subscriber_id`),
  CONSTRAINT `fk-video_subscriber_like-subscriber_id` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video_subscriber_like-video_id` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_subscriber_like`
--

LOCK TABLES `video_subscriber_like` WRITE;
/*!40000 ALTER TABLE `video_subscriber_like` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_subscriber_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_subscriber_unlike`
--

DROP TABLE IF EXISTS `video_subscriber_unlike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_subscriber_unlike` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-video_subscriber_unlike-video_id` (`video_id`),
  KEY `idx-video_subscriber_unlike-subscriber_id` (`subscriber_id`),
  CONSTRAINT `fk-video_subscriber_unlike-subscriber_id` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video_subscriber_unlike-video_id` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_subscriber_unlike`
--

LOCK TABLES `video_subscriber_unlike` WRITE;
/*!40000 ALTER TABLE `video_subscriber_unlike` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_subscriber_unlike` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_tag`
--

DROP TABLE IF EXISTS `video_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-video_tag-video_id` (`video_id`),
  KEY `idx-video_tag-tag_id` (`tag_id`),
  CONSTRAINT `fk-video_tag-tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video_tag-video_id` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_tag`
--

LOCK TABLES `video_tag` WRITE;
/*!40000 ALTER TABLE `video_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video_view`
--

DROP TABLE IF EXISTS `video_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx-video_view-video_id` (`video_id`),
  KEY `idx-video_view-subscriber_id` (`subscriber_id`),
  CONSTRAINT `fk-video_view-subscriber_id` FOREIGN KEY (`subscriber_id`) REFERENCES `subscriber` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-video_view-video_id` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video_view`
--

LOCK TABLES `video_view` WRITE;
/*!40000 ALTER TABLE `video_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `video_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'newjavon'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-30 15:26:49
