
function dealCards() {

    /**
     * use the randomized cards and give to players
     */
    var curPlayer = 1;
    cards.forEach(function (curCard) {
        dealedCards[curPlayer] += showCard(curCard) + ',';
        if (curPlayer == numPlayer) {
            curPlayer = 1;
        } else {
            curPlayer++;
        }
    });

    /**
     * show the dealed cards in app container using jquery
     */
    ctr = 1;
    dealedCards.forEach(function (card) {
        appContent = '<div>Player ' + ctr + ' cards:&nbsp;&nbsp;';
        appContent += card;
        appContent += '</div>';
        ctr++;
        $('#app').append(appContent);
    });
}

/**
 * naming the card from numbers from 1 to 13
 */
function showCard(card) {
    var rem = card % 13;
    if (rem === 0) {
        return cardType(card) + 'K';
    } else if (rem === 12) {
        return cardType(card) + 'Q';
    } else if (rem === 11) {
        return cardType(card) + 'J';
    } else if (rem === 10) {
        return cardType(card) + 'X';
    } else if (rem === 1) {
        return cardType(card) + 'A';
    } else {
        return cardType(card) + rem;
    }
}

/**
 * check which type of card
 */
function cardType(card) {
    if (card >= 40) { //diamond
        return 'D';
    }
    if (card >= 27) { //clubs
        return 'C';
    }
    if (card >= 14) { //spades
        return 'S';
    }
    if (card >= 0) { //heart
        return 'H';
    }
}

/**
 * create an empty storage for dealed cards
 */
function initDealedCards() {
    var dealedCards = [];
    for (ctr = 1; ctr <= numPlayer; ctr++) {
        dealedCards[ctr] = '';
    }
    return dealedCards;
}

/**
 * create storage for cards from 1 to 52
 */
function initCards() {
    var card = [];

    //init 52 cards
    for (ctr = 0; ctr < 52; ctr++) {
        card[ctr] = ctr + 1;
    }
    return card;
}

/**
 * create storage for players
 */
function initPlayers() {
    var ctr, player = [];
    for (ctr = 0; ctr < numPlayer; ctr++) {
        player[ctr] = ctr + 1;
    }
    return player;
}

/**
 * shuffle the 52 cards
 */
function shuffle(array) {
    let currentIndex = array.length,
        randomIndex;

    // While there remain elements to shuffle.
    while (currentIndex != 0) {

        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]
        ];
    }

    return array;
}