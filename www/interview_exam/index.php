<?php
if (isset($_POST['numPlayer']) && $_POST['numPlayer'] != '') {
    $numPlayer = $_POST['numPlayer'];
} else {
    $numPlayer = '';
}
?>
<!DOCTYPE html>
<html>
<head><title>Interview Exam</title></head>
<body>
    <div>
        <form method="post">
            <input type="number" name="numPlayer" value="<?php echo $numPlayer; ?>" placeholder="enter number of players" />
            <input type="submit" value="Play" />
        </form>
    </div>
    <div id="app"></div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <?php
    if( isset($_POST['numPlayer']) ){
        if (is_numeric($numPlayer) && floor($numPlayer) == $numPlayer && $numPlayer > 0) {
            echo '<script src="index.js"></script>';
            echo '<script>';
            echo '$(document).ready(function() {';
            echo 'dealCards();';
            echo '});';
            echo '
                    var ctr,
                    numPlayer = ' . $numPlayer . ',
                    cards = initCards(),
                    players = initPlayers(),
                    dealedCards = initDealedCards(),
                    appContent;

                    //randomize/shuffle the cards
                    shuffle(cards);
            ';
            echo '</script>';
        } else {
            echo '<script>';
            echo 'alert("Input value does not exist or value is invalid");';
            echo '</script>';
        }
    }
    ?>
</body>
</html>